output "lb_tg" {
  value = aws_lb_target_group.node_lb_tg
}

output "lb_listener" {
  value = aws_lb_listener.listener
}
