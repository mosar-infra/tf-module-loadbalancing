# loadbalancing/main.tf

resource "aws_lb" "lb" {
  name               = var.lb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.public_security_group_ids
  subnets            = var.public_subnet_ids
  idle_timeout       = 100
}


resource "aws_lb_target_group" "node_lb_tg" {
  for_each = var.lb_target_groups
  name     = "${each.key}-lb-tg-${substr(uuid(), 0, 3)}"
  port     = each.value.port
  protocol = each.value.protocol
  target_type = each.value.target_type
  vpc_id   = var.vpc_id
  health_check {
    healthy_threshold   = each.value.healthy_threshold
    unhealthy_threshold = each.value.unhealthy_threshold
    path                = each.value.path
    timeout             = each.value.timeout
    interval            = each.value.interval
  }
  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = var.listener_port
  protocol          = var.listener_protocol
  ssl_policy        = var.ssl_policy
  certificate_arn   = var.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.node_lb_tg[var.lb_listener_target_group].arn
  }
}


