# loadbalancing/variables.tf

variable "lb_name" {}
variable "public_subnet_ids" {}
variable "public_security_group_ids" {}
variable "environment" {}
variable "vpc_id" {}
variable "lb_target_groups" {}
variable "listener_port" {}
variable "listener_protocol" {}
variable "ssl_policy" {}
variable "certificate_arn" {}
variable "lb_listener_target_group" {}
